# Odoo
# 1. Installing Odoo 14
``` bash
./start.sh
```

Visit the site at: http://localhost:8080

![odoo-14-welcome-docker](screenshots/odoo-14-welcome-screenshot.png)

![odoo-14-apps-docker](screenshots/odoo-14-apps-screenshot.png)

![odoo-14-sales](screenshots/odoo-14-sales-screen.png)

![odoo-14-form](screenshots/odoo-14-sales-form.png)

## 2. Remove Odoo
``` bash
./remove.sh
```