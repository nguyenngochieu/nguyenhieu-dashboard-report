# -*- coding: utf-8 -*-
{
    'name': "WeUp - Báo cáo",
    'summary': """Báo cáo""",
    'description': """Báo cáo""",
    'author': "ERP - WeUp",
    'website': "https://weupgroup.vn",
    'category': 'WeUp',
    'version': '1.0',
    'depends': [
        'base','board' ,'product', 'crm', 'contacts', 'sale_management', 'account','abm'
    ],
    'data': [
        'security/ir.model.access.csv',
        'views/list_report_view.xml',
        'views/crm_report_view.xml',
        'views/account_invoice_report_view.xml',
       
    ],
    'qweb': [],
    'installable': True,
    'application': True,
}
# vim:expandtab:smartindent:tabstop=4:softtabstop=4:shiftwidth=4: