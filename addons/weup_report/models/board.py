from openerp import SUPERUSER_ID
from openerp.osv import fields, osv

from operator import itemgetter
from textwrap import dedent

class board(osv.osv):

    _inherit = 'board.board'

    def fields_view_get(self, cr, user, view_id=None, view_type='form', context=None, toolbar=False, submenu=False):

        user = SUPERUSER_ID
        res = {}
        res = super(board, self).fields_view_get(cr, user, view_id, view_type,
                                                       context=context, toolbar=toolbar, submenu=submenu)

        CustView = self.pool.get('ir.ui.view.custom')
        vids = CustView.search(cr, user, [('ref_id', '=', view_id)], context=context)
        if vids:
            view_id = vids[0]
            arch = CustView.browse(cr, user, view_id, context=context)
            res['custom_view_id'] = view_id
            res['arch'] = arch.arch
        res['arch'] = self._arch_preprocessing(cr, user, res['arch'], context=context)
        res['toolbar'] = {'print': [], 'action': [], 'relate': []}

        return res

class board_create(osv.osv_memory):

    _inherit = 'board.create'

    def board_create(self, cr, uid, ids, context=None):
        assert len(ids) == 1

        uid = SUPERUSER_ID
        res = super(board_create, self).board_create(cr, uid, ids, context=None)

        return res