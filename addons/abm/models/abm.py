from odoo import api, fields, models, tools, _
from odoo.exceptions import UserError, ValidationError

class AbmModel(models.Model):
    _name = 'abm.demo'
    _inherit = ['mail.thread', 'mail.activity.mixin']
    _description = 'Bảng dữ liệu quản lý lịch khám chữa bệnh'

    # Thêm các trường
    name = fields.Char('Tên cuộc hẹn', required=True)
    # Khách hàng
    partner_id = fields.Many2one('res.partner', string='Khách hàng', required=True)
    phone = fields.Char(string='Điện thoại', compute='_get_partner_phone', inverse="_inverse_partner_phone")
    mobile = fields.Char(string='Di động', compute='_get_partner_mobile', inverse="_inverse_partner_mobile")
    email = fields.Char(string='Email', compute='_get_partner_email', inverse="_inverse_partner_email")
    street = fields.Char(string='Địa chỉ', compute='_get_partner_street', inverse="_inverse_partner_street")
    year_of_birth = fields.Integer(string='Năm sinh', compute='_get_partner_year_of_birth', inverse="_inverse_partner_year_of_birth")
    street2 = fields.Char(string='Phường/Xã', compute='_get_partner_street2', inverse="_inverse_partner_street2")
    city = fields.Char(string='Quận/Huyện', compute='_get_partner_city', inverse="_inverse_partner_city")
    state_id = fields.Many2one('res.country.state', string='Tỉnh/Tp', compute='_get_partner_state', inverse="_inverse_partner_state")
    zip = fields.Char(string='Mã bưu', compute='_get_partner_zip', inverse="_inverse_partner_zip")
    country_id = fields.Many2one('res.country', string='Quốc gia', compute='_get_country_name', inverse="_inverse_country_name")
    # Thông tin lịch khám
    specialty_id = fields.Many2one('hr.employee.specialty', string='Chuyên khoa')
    appoiment_address = fields.Many2one('res.company.base', string = 'Cơ sở khám', required=True)
    date_start = fields.Datetime(string='Thời gian khám', required=True)
    disease_manifestation = fields.Text('Mô tả tình trạng bệnh')

    # Thông tin bác sĩ khám
    physician_id = fields.Many2one('hr.employee', domain="[('job_id','=','Bác sĩ')]", string='Bác sĩ')
    department = fields.Many2one('hr.department', string = 'Buồng khám')
    disease_description = fields.Text('Đầu bệnh')
    physician_note = fields.Text('Kết quả')
    disease_id = fields.Many2one('x_disease', string="Đầu bệnh chính")
    disease_ids = fields.Many2many('x_disease', string="Đầu bệnh phụ")
    disease_detail = fields.Text('Chi tiết bệnh')
    comment = fields.Text("Ghi chú")

    # tình trạng
    stage_id = fields.Many2one('abm.stage', string="Tình trạng", required=True)
    fail_id = fields.Many2one('abn.stage.fail', string="Lí do thất bại")
    note = fields.Text("Ghi chú")

    sale_order_id = fields.Many2one('sale.order')
    company_id = fields.Many2one('res.company', default=lambda self: self.env.company.id if self.env.company.id else None, string="Công ty")

    @api.depends('partner_id')
    def _get_partner_phone(self):
        if not self.phone or self.partner_id.phone:
            self.phone = self.partner_id.phone
    @api.depends('partner_id')
    def _get_partner_mobile(self):
        if not self.mobile or self.partner_id.mobile:
            self.mobile = self.partner_id.mobile
    @api.depends('partner_id')
    def _get_partner_email(self):
        if not self.email or self.partner_id.email:
            self.email = self.partner_id.email
    @api.depends('partner_id')
    def _get_partner_street(self):
        if not self.street or self.partner_id.street:
            self.street = self.partner_id.street
    @api.depends('partner_id')
    def _get_partner_street2(self):
        if not self.street2 or self.partner_id.street2:
            self.street2 = self.partner_id.street2
    @api.depends('partner_id')
    def _get_partner_city(self):
        if not self.city or self.partner_id.city:
            self.city = self.partner_id.city
    @api.depends('partner_id')
    def _get_partner_state(self):
        if not self.state_id or self.partner_id.state_id.id:
            self.state_id = self.partner_id.state_id.id
    @api.depends('partner_id')
    def _get_partner_state(self):
        if not self.state_id or self.partner_id.state_id.id:
            self.state_id = self.partner_id.state_id.id
    @api.depends('partner_id')
    def _get_partner_state(self):
        if not self.state_id or self.partner_id.state_id.id:
            self.state_id = self.partner_id.state_id.id
    @api.depends('partner_id')
    def _get_partner_zip(self):
        if not self.zip or self.partner_id.zip:
            self.zip = self.partner_id.zip
    @api.depends('partner_id')
    def _get_partner_year_of_birth(self):
        if not self.year_of_birth or self.partner_id.x_year_of_birth:
            self.year_of_birth = self.partner_id.x_year_of_birth
    @api.depends('state_id')
    def _get_country_name(self):
        if self.state_id:
            self.country_id = self.state_id.country_id

    def _inverse_country_name(self):
        if not self.state_id.country_id or self.country_id:
            self.state_id.country_id = self.country_id
    def _inverse_partner_phone(self):
        if not self.partner_id.phone or self.phone:
            self.partner_id.phone = self.phone
    def _inverse_partner_mobile(self):
        if not self.partner_id.mobile or self.mobile:
            self.partner_id.mobile = self.mobile
    def _inverse_partner_email(self):
        if not self.partner_id.email or self.email:
            self.partner_id.email = self.email
    def _inverse_partner_street(self):
        if not self.partner_id.street or self.street:
            self.partner_id.street = self.street
    def _inverse_partner_street2(self):
        if not self.partner_id.street2 or self.street2:
            self.partner_id.street2 = self.street2
    def _inverse_partner_city(self):
        if not self.partner_id.city or self.city:
            self.partner_id.city = self.city
    def _inverse_partner_state(self):
        if not self.partner_id.state_id.id or self.state_id.id:
            self.partner_id.state_id.id = self.state_id.id
    def _inverse_partner_zip(self):
        if not self.partner_id.zip or self.zip:
            self.partner_id.zip = self.zip
    def _inverse_partner_year_of_birth(self):
        if not self.partner_id.x_year_of_birth or self.year_of_birth:
            self.partner_id.x_year_of_birth = self.year_of_birth

    def action_create_invoicing(self):
        self.ensure_one()
        action = self.env["ir.actions.actions"]._for_xml_id("sale.action_quotations_salesteams")
        action['context'] = {
            'default_partner_id': self.partner_id.id,
            'default_name': self.partner_id.name,
            'default_phone': self.phone,
        }
        return action

class PartnerModel(models.Model):
    _inherit = 'res.partner'

    x_total = fields.Integer('Số lần khám')
    x_year_of_birth = fields.Integer('Năm sinh')
    x_disease_id = fields.Many2one('x_disease', string="Đầu bệnh chính")
    x_disease_ids = fields.Many2many('x_disease', string="Đầu bệnh phụ")
    x_is_old_partner = fields.Boolean('Là bệnh nhân cũ')
    x_create_date = fields.Char('Ngày khám')
    x_note = fields.Text('Ghi chú bệnh nhân cũ')
    x_appointment_id = fields.Many2many('abm.demo', string="Lịch khám")

    # Button bấm xong chuyển sang bên đặt lịch với các data từ bên liên hệ
    def action_appointment_booking(self):
        self.ensure_one()
        action = self.env["ir.actions.actions"]._for_xml_id("abm.action_abm")
        action['context'] = {
            'default_partner_id': self.id,
            'default_phone': self.phone,
            'default_mobile': self.mobile,
            'default_email': self.email,
            'default_street': self.street,
            'default_street2': self.street2,
            'default_city': self.city,
            'default_state_id': self.state_id.id,
            'default_country_id': self.country_id.id,
            'default_year_of_birth': self.x_year_of_birth,
        }
        return action

class HrModel(models.Model):
    _inherit = 'hr.employee'

    x_specialty_id = fields.Many2one('hr.employee.specialty', string="Chuyên khoa")
    x_base_company_id = fields.Many2one('res.company.base', string="Cơ sở")

class AbmStageModel(models.Model):
    _name = 'abm.stage'
    _description = 'Bảng tình trạng của lịch khám'

    # Thêm các trường
    name = fields.Char('Tên tình trạng', required=True)
    company_id = fields.Many2one('res.company', default=lambda self: self.env.company.id if self.env.company.id else None, string="Công ty")

class DiseaseModel(models.Model):
    _name = 'x_disease'

    # Thêm các trường
    name = fields.Char('Đầu bệnh', required=True)
    description = fields.Char('Mô tả')
    company_id = fields.Many2one('res.company', default=lambda self: self.env.company.id if self.env.company.id else None, string="Công ty")

class AbmStageFailModel(models.Model):
    _name = 'abm.stage.fail'
    _description = 'Lý do hủy lịch khám'

    # Thêm các trường
    name = fields.Text('Lí do', required=True)
    company_id = fields.Many2one('res.company', default=lambda self: self.env.company.id if self.env.company.id else None, string="Công ty")

class SpecialtyModel(models.Model):
    _name = 'hr.employee.specialty'
    _description = 'Bảng chuyên khoa của bệnh viện/phòng khám'

    # Thêm các trường
    name = fields.Char('Tên chuyên khoa', required=True)
    company_id = fields.Many2one('res.company', default=lambda self: self.env.company.id if self.env.company.id else None, string="Công ty")

class CompanyModel(models.Model):
    _inherit='res.company'

    x_base_company_ids = fields.One2many('res.company.base', 'parent_id', string="Cơ sở")

class BaseCompanyModel(models.Model):
    _name = 'res.company.base'
    _description = 'Cơ sở khác của công ty'

    # Thêm các trường
    name = fields.Char('Tên cơ sở', required=True)
    phone = fields.Char('Số điện thoại')
    mobile = fields.Char('Di động')
    email = fields.Char('Email')
    street = fields.Char('Địa chỉ')
    street2 = fields.Char('Phường/Xã')
    city = fields.Char('Quận/Huyện')
    state_id = fields.Many2one('res.country.state', string='Tỉnh/Tp')
    zip = fields.Char('Mã bưu')
    country_id = fields.Many2one('res.country', string='Quốc gia', compute='_get_country_name', inverse="_inverse_country_name")
    parent_id = fields.Many2one('res.company', string='Công ty mẹ')
    company_id = fields.Many2one('res.company', default=lambda self: self.env.company.id if self.env.company.id else None, string="Công ty")

    @api.depends('state_id')
    def _get_country_name(self):
        if self.state_id:
            self.country_id = self.state_id.country_id

    def _inverse_country_name(self):
        if not self.state_id.country_id or self.country_id:
            self.state_id.country_id = self.country_id
