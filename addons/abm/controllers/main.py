import odoo
import json

class AbmModel(odoo.http.Controller):
    @odoo.http.route('/abm', auth='public')
    def abmIndex(self):
        return json.dumps({
            'content': "Welcome to 'bar' API"
        })

    @odoo.http.route(['/abm/<db_name>/<id>'], auth='none', type="http", sitemap=False, cors='*', csrf=False)
    def getData(self, dbname, id, **kw):
        model_name = "abm.demo"
        try:
            registry = odoo.modules.registy.Registry(dbname)
            with odoo.api.Environment.manage(), registry.cursor() as cr:
                env = odoo.api.Environment(cr, odoo.SUPERUSER_ID, {})
                rec = env[model_name].search([('id', '=', int(id))], limit=1)
                response = {
                    "status": "success",
                    "data": {
                        "name": rec.name,
                        "partner_name": rec.partner_name,
                        "phone": rec.phone,
                        "mobile": rec.mobile,
                        "street": rec.street,
                        "date_of_birth": rec.date_of_birth,
                        "gender": rec.gender,
                        "specialist": rec.specialist,
                        "appoiment_address": rec.appoiment_address,
                        "date_start": rec.date_start,
                        "date_end": rec.date_end,
                        "disease_manifestation": rec.disease_manifestation,
                        "appointment_note": rec.appointment_note,
                        "physician": rec.physician,
                        "department": rec.department,
                        "disease_description": rec.disease_description,
                        "physician_note": rec.physician_note,
                    }
                }
        except Exception:
            response = {
                "status": "error",
                "content": "not found"
            }
        return json.dumps(response)