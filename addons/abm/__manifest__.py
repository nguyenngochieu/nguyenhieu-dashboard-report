{
    'name': "Quản lý Lịch khám",
    'summary': """Quản lý Lịch khám""",
    'description': """Quản li lịch khám chữa bệnh của bác sĩ, bệnh nhân""",
    'author': "thuongnd@weupgroup.vn",
    'website': "https://weupgroup.vn",
    'category': 'Uncategorized',
    'version': '1.0',
    'depends': [
        'contacts', 'hr', 'sale_management'
    ],
    'data': [
        'security/ir.model.access.csv',
        'views/abm_views.xml',
        'views/hr_employee_specialty_views.xml',
        'views/x_disease_view.xml',
        'views/res_company_base_view.xml'
    ],
    'qweb': [],
    'installable': True,
    'application': True,
}