#!/bin/bash
# completely delete/reset Odoo container & data
docker-compose down
sudo rm -rf postgresql
sudo rm -rf etc/addons
sudo rm -rf etc/filestore
sudo rm -rf etc/sessions
sudo rm -rf etc/odoo-server.log
echo 'Odoo deleted!'
